--- Wifi Obj def & handlers

-- obj:new(nil, {ssid = "IoT", pass = "IoT"})
function wifi:new(obj, tbl)
   obj = obj or {}
   setmetatable(obj, self)
   self.__index = self
   self.ip = "0.0.0.0"
   self.netmask = "0.0.0.0"
   self.gateway = "0.0.0.0"
   self.state = nil
   self.ssid = "" or tbl.ssid
   self.pass = "" or tbl.pass
   self.channel = 0
   self.save = false
   self.retries = 10
   return obj
end

-- obj:set_ip
function wifi:set_intfo()
   local info = net.ifinfo(0) --0 is STA index

   self.ip = info.ip
   self.netmask = info.netmask
   self.gateway = info.gateway
end

-- obj:get_state
function wifi:get_state()
   return self.state
end

-- obj:ping("www.google.com")
function wifi:ping(host)
   --net.ping hasn't been added to nodemcu, so this is a work around
   net.dns.resolve(host, function(sk, ip)
					  if (ip == nil) then
						 self.state = "Disconnected"
					  else
						 self.state = "Connected"
					  end
   end)

   if self.state == "Connected" then
	  return true
   elseif self.state == "Disconnected" then
	  return false
   end
end

function wifi:connect()
   wifi.setmode(wifi.STATION)
   --802.11b chosen for additional wifi range
   wifi.setphymode(wifi.PHYMODE_B)
   
   --Configure wireless connection & connect
   wifi.sta.config({ssid = self.ssid,
					pwd = self.pass
					channel = self.channel
					save = self.save})
   
   --Set IP info from connection
   self:set_intfo()
   
   --Check if we can reach out from the new network connection
   self:ping("www.google.com")

   --Check if our connection was successful, if not decress retry attempts
   if (self.state ~= "Connected") and (self.retries >= 1) then
	  self.retries = self.retries - 1
	  tmr.alarm(0, 2500, 0, self:connect)
   end
end

--- MQTT Functions
-- obj:new(nil, {id = "SecTem01", user = "IoT", pass = "IoT", host = "192.168.88.123", timeout = 60, interval = 5, port = 1234, topic = "/temp"})
function mqtt:new(obj, tbl)
   obj = obj or {}
   setmetatable(obj, self)
   self.__index = self
   self.id = "SecTem01" or tbl.id
   self.user = "IoT" or tbl.user
   self.pass = "IoT" or tbl.pass
   self.host = "0.0.0.0" or tbl.host
   self.timeout = 120 or tbl.timeout
   self.interval = 1 or tbl.interval
   self.topic = "/temp" or tbl.topic
   self.port = 1883 or tbl.port
   self.broker = nil
   self.state = nil
   self.reason = nil
   return obj
end

function mqtt:connect()
   self.broker:connect(self.host, self.port, 0,
					   function(client)
						  self.state = "Connected"
					   end,
					   function(client, reason)
						  self.reason = reason
   end)
end

function mqtt:reconnect()
   self:connect
end

function mqtt:pub()
   data = adc.read(0) --This may need to change depending on sensor
   retval = self.id .. " " .. data

   self.broker:publish(self.topic, retval, 0, 0)
end

function mqtt:sub()
end

function mqtt:create_connection()
   self.broker = mqtt.Client(self.id, self.timeout, self.user, self.pass, 1)

   --Setup callbacks
   self.broker:on("connect", function(client)
					 self.state = "Connected"
   end)

   self.broker:on("offline", self:reconnect)
end

--- Data Aggregation Functions

--Create main function loop
loop = tmr.create()
loop:register(10000, tmr.ALARM_AUTO, function ()
				 if wconn:get_state == "Connected" then
					--Connect to the broker
					mconn:connect()
					--Publish data
					mconn:pub()
				 else
					--Turn on the led to indicate network failure if we reach failure with 10 retries
					gpio.write(3, gpio.LOW)
					--Cause a loop delay to allow intervention
					tmr.delay(1000000)
				 end
end)

-- Entrypoint of sector-temp.lua
wconn = wifi:new(nil, {ssid = "IoT",
					   pass = "IoT"})
mconn = mqtt:new(nil, {id = "SecTem01",
					   user = "IoT",
					   pass = "IoT",
					   host = "192.168.88.123",
					   timeout = 120,
					   interval = 1,
					   port = 1883,
					   topic = "/sector_temp"})
mconn:create_connection()
wconn:connect()
loop:start()
