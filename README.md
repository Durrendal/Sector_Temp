# What?

A simple ESP8266 NodeMCU based local temperature sensor project, used to teach my son about electronics and Lua.

## Hardware

ESP8266-ESP12F
TMP36

## TMP36 to ESP8266

The TMP36 is an analog sensor that emits voltage equivalent to the ambient temperature in celsius.

Wiring this to the ESP8266 is accomplished as follows
```
TMP36 v+ -> ESP8266 3.3v
TMP36 vOut -> ESP8266 A0
TMP36 Gnd -> ESP8266 GND
```

![ESP8266 to TMP36 Wiring](.img/wiring.jpg)

Connecting to the esp8266 from an Alpine linux device can be done like so:
```
sudo screen /dev/serial/by-id/$serial_id_of_board 115200
```
Admittedly we had some difficulties doing this initially, we'd start the serial connection only to be greated with jumbled output. If that occurs the fix we found was..

Reading data from the sensor is as simple as doing
```
adc.read(0)
```
